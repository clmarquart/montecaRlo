###
# produces errors
###
produceError = function(
  n, 
  alpha = 0.05, handSetBaserate = 0.2, fullSetBaserate = 0.1, 
  handSetLength = 30, fullSetLength = 10000, runs = 800, minPrecision = 0.6, 
  maxPrecision = 1, minKappa = 0.3, maxKappa = 1, kappaThreshold = 0.65, 
  kappaMinRho = 0.4, type = 'KAPPA', methods = c("RHO","COMMON","BOOK"),
  progressOutput = NULL, progressOutOf = 0, FUN = NULL
){
  if (!is.null(progressOutput)) {
    writeBin(1/progressOutOf, progressOutput)
  }
  ourTypeOne = 0
  ourTypeTwo = 0
  goodSet = 0
  hits = 0
  
  commonTestHit = 0
  commonTypeOne = 0
  commonTypeTwo = 0
  
  bookTestHit = 0
  bookTypeOne = 0
  bookTypeTwo = 0
  
  thisHandPvalue=1
  
  KPs = get('KPs', envir = .GlobalEnv)
  KP = KPs[[n]]
  kappa = KP$kappa
  precision = KP$precision
  recall = getR(kappa, fullSetBaserate, precision)
  #return(KP)
  thisSet = prset(precision = precision, recall = recall, length = fullSetLength, baserate = fullSetBaserate);
  
  #determines if the set is actually above the threshold or not
  if(type == 'PERCENT'){
    thisSetOK = calcPA(thisSet);
  }else{
    thisSetOK = calcKappa(thisSet);
  }
  thisHandKappa = -1;
  trueYes = (thisSetOK > kappaThreshold)
  
  #common method (take handset and see if that is above the threshold or not)
  if(length(which(methods == "COMMON")) == 1){
    thisHandSet = getHandSet(set = thisSet, len = handSetLength, rate = 0, returnSet = TRUE)
    if(type == 'PERCENT'){
      thisHandKappa = calcPA(thisHandSet)
    }else{
      thisHandKappa = calcKappa(thisHandSet)
    }
    commonYes = (thisHandKappa > kappaThreshold)
  }else{
    commonYes = 0
  }

  #our method (take handset compute kappa and then calculate rho from the observed kappa and see if that is below alpha)
  if(type == 'PERCENT'){
    bookYes = 0
    ourYes = 0
    thisHandPvalue = 0
  }else{
    if(length(which(methods == "RHO")) == 1) {
      thisHandSet = getHandSet(set = thisSet, len = handSetLength, rate = handSetBaserate, returnSet = TRUE)
      thisHandKappa = calcKappa(thisHandSet)
      thisHandPvalue = calcRho(
        observedKappa = thisHandKappa,
        handSetBaserate = handSetBaserate,
        fullSetBaserate = fullSetBaserate,
        handSetLength = handSetLength,
        fullSetLength = fullSetLength,
        runs = runs,
        kappaThreshold = kappaThreshold,
        kappaMin = kappaMinRho,
        minPrecision,
        maxPrecision
      )
      ourYes = (thisHandPvalue < alpha)
    }else{
      ourYes = 0
      thisHandPvalue = 0
    }
    if(length(which(methods == "BOOK")) == 1){
      #book method
      thisHandSet2 = getHandSet(set = thisSet, len = handSetLength, rate = 0, returnSet = TRUE)
      thisBookPvalue = kappaAsymVarP(thisHandSet2, kappaThreshold)
      bookYes = (thisBookPvalue < alpha)
    }else{
      bookYes = 0
    }
  }
  
  #determine which were errors
  if ( ourYes ) {hits = 1}
  if ( trueYes) {goodSet = 1}
  if ( !trueYes & ourYes) {ourTypeOne = 1}
  if ( trueYes & !ourYes) {ourTypeTwo = 1}
  if ( commonYes) {commonTestHit = 1}
  if ( !trueYes & commonYes) {commonTypeOne = 1}
  if ( trueYes & !commonYes) {commonTypeTwo = 1}
  if ( bookYes ) {bookTestHit = 1}
  if( !trueYes & bookYes) { bookTypeOne = 1}
  if( trueYes & !bookYes) { bookTypeTwo = 1}
  
  names = c("our type 1", "common type 1", "book type 1", "our type 2", "common type 2", "book type 2", "total miss", "total hits", "Kappa", "hand Kappa"," 95%")
  results = t(as.matrix(c(ourTypeOne, commonTypeOne, bookTypeOne, ourTypeTwo, commonTypeTwo, bookTypeTwo, (1 - goodSet), goodSet, thisSetOK, thisHandKappa, thisHandPvalue)))
  colnames(results) = names
  
  return(results)
}