#' Wrapper for for using a parallel version of produce error
#' call in generate.table call
#'
#' @return
#' @export
#'
par.generate.table = function(...) {
  library(parallel)
  
  genTableResult = generate.table(..., FUN = par.produce.error);
  
  return(genTableResult);
}