#' Title
#'
#' @param currBR 
#' @param currHSL 
#' @param setLength 
#' @param runs 
#' @param minPrecision 
#' @param maxPrecision 
#' @param Kmin 
#' @param Kmax 
#' @param kappaThreshold 
#' @param kappaMinRho 
#' @param type 
#' @param handSetBaserate 
#' @param methods 
#' @param tests 
#' @param maxOut 
#' @param trackProgress 
#'
#' @return
#' @export
#'
#' @examples
par.produce.error = function(
  currBR, 
  currHSL, 
  setLength, 
  runs, 
  minPrecision, 
  maxPrecision, 
  Kmin, 
  Kmax, 
  kappaThreshold, 
  kappaMinRho,type, 
  handSetBaserate, 
  methods, 
  tests, 
  maxOut,
  KPs = NULL,
  trackProgress = TRUE
) {
  library('parallel')
  
  numCores = detectCores()
  if(maxOut){
    numWorkers = numCores
  }else{
    numWorkers = numCores - 1
  }
  
  OS = Sys.info()['sysname']
  if(OS == "Linux"){
    clusterType = "FORK"
  }else{
    clusterType = "PSOCK"
  }
  
  cl = makeCluster(numWorkers, type = clusterType)
  clusterExport(cl, c(
    'calcRho','checkBRPKcombo','genPKcombo','getBootPvalue',
    'getHandSet',"getR","kappaAsymVarP","calcKappa","prset",
    "randomKappaSet","genPcombo","KPs","generateKPs","calcPA"
  ))
  
  Sys.sleep(0.05)
  
  print("calling parLapply")
  
  res = parLapply(cl, tests, produce.error, fullSetBaserate = currBR, handSetLength = currHSL, fullSetLength = setLength, runs = runs, minPrecision = minPrecision, maxPrecision = maxPrecision, minKappa = Kmin, maxKappa = Kmax, kappaThreshold = kappaThreshold, kappaMinRho = kappaMinRho, type = type, handSetBaserate = handSetBaserate, methods = methods)
  
  stopCluster(cl)
  closeAllConnections()
   
  return(res);
}