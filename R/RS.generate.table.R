#' Wrapper for sending generate.table call to
#' an instance of RServe
#'
#' @return
#' @export
#'
#' @examples
RS.generate.table = function(..., rs.host = "10.128.254.27", rs.port = 6311) {
  library(RSclient);
  library(parallel)
  
  args = list(...)
  
  genTableResult = generate.table(..., FUN = function(currBR, currHSL, handSetBaserate, kappaThreshold, kappaMinRho,
      Kmin, Kmax, maxOut, maxPrecision, methods = methods, minPrecision, runs, setLength, tests, type, KPs
    ){
    con = try(RS.connect(host = rs.host, port = rs.port), silent=FALSE);
    
    ## Halt if not successful connecting
    if(class(con) != "RserveConnection") {
      print("Error connecting to RServe");
      return();
    }
   
    lapply(
      c(
        #"calcKappa", "calcPA","calcRho","checkBRPKcombo","currBR","currHSL",
        #"generate.table","genPcombo","genPKcombo","genPKcomboNEW","generateKPs",
        #"getHandSet","getBootPvalue","getR","handSetBaserate","kappaAsymVarP",
        #"kappaThreshold","kappaMinRho","Kmin","Kmax","KPs",
        #"methods",
        #"maxPrecision","minPrecision","prset","parLapply","produce.error",
        #"randomKappaSet","runs","setLength","tests","type",
        "tests", "runs",
        "StatisticalMethod", "BookMethod",
        "calcKappa","kappaThreshold",
        "methods",
        "setLength","type",
        "KPs","getR","prset",
        "currHSL","currBR",
        "maxOut","tests",
        "produce.error",
        "RS.produce.error"
      ), FUN=function(a) {
        RS.assign(con, a, get(a)) 
      }
    ) 
    RS.eval(rsc = con, library('parallel'));
    
    res = RS.eval( 
      rsc = con,
      RS.produce.error(
        currBR = currBR, currHSL = currHSL, setLength = setLength, runs = runs, 
        minPrecision = minPrecision, maxPrecision = maxPrecision, Kmin = Kmin, 
        Kmax = Kmax, kappaThreshold = kappaThreshold, kappaMinRho = kappaMinRho, 
        type = type, handSetBaserate = handSetBaserate, methods = methods, 
        tests = tests, maxOut = maxOut
      )
    );
    
    closeAllConnections()
    
    RS.close(con);
    return(res);
  })
  
  return(genTableResult);
}